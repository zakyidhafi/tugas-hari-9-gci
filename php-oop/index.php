<?php  
require_once 'Animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

$sheep = new Animal("shaun");

echo "Name : ".$sheep->getName(); // "shaun"
echo "legs: ".$sheep->getLegs(); // 2
echo "cold blooded: ".$sheep->getCold_Blooded(); // false

echo "<br>";

$kodok = new Frog("buduk");
echo "Name: ".$kodok->getName(); // "kera sakti"
echo "legs: ".$kodok->getLegs(); // 4
echo "cold blooded: ".$kodok->getCold_Blooded(); // false
echo "Jump: ".$kodok->jump() ; // "hop hop"

echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name: ".$sungokong->getName(); // "kera sakti"
echo "legs: ".$sungokong->getLegs(4); // 4
echo "cold blooded: ".$sungokong->getCold_Blooded(); // false
echo "Yell: ".$sungokong->yell(); // "Auooo"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>

